package com.coderknock.service;

import com.alibaba.fastjson.JSON;
import com.coderknock.jedis.KeyGen;
import com.coderknock.jedis.executor.JedisExecutor;
import com.google.common.base.Joiner;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.Tuple;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-09-03
 * @QQGroup 213732117
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
public class TopService {
    static final Logger LOG = Logger.getLogger(TopService.class.getName());
    Random random = new Random(1000);
    //获取系统的换行符
    String lineSeparator = System.getProperty("line.separator", "\n");
    /**
     * 1、增长
     * 2、查看
     */

    /**
     * 增加一个用户在某个标签下的积分
     * 如果该用户存在则值增加该用户分数
     * 如果该用户不存在则添加该用户及其分数
     * 默认是今天
     *
     * @param tag
     * @param uid   用户的ID
     * @param score 增加的分数
     * @return
     */
    public void incrTag(String tag, String uid, double score) {
        LocalDateTime date = LocalDateTime.now();
        incrTag(tag, uid, score, date);
    }

    /**
     * 增加一个用户在某个标签下的积分
     * 如果该用户存在则值增加该用户分数
     * 如果该用户不存在则添加该用户及其分数
     *
     * @param tag
     * @param uid   用户的ID
     * @param score 增加的分数
     * @param date  增加的时间
     * @return
     */
    public void incrTag(String tag, String uid, double score, LocalDateTime date) {
        //标签积分
        final String tagKey = KeyGen.TAG_TOP_SCORE.genKey(tag);
        //周积分
        final String weekKey = KeyGen.WEEK_TOP_SCORE.genDateKey(date);
        //标签月度积分
        final String tagMonthKey = KeyGen.TAG_TOP_SCORE.genKeyWithDate(tag, date);
        //日期积分   今日排行 周排行
        final String dateKey = KeyGen.DATE_TOP_SCORE.genDateKey(date);
        //月度积分
        final String mouthKey = KeyGen.MONTH_TOP_SCORE.genDateKey(date);
        LOG.info("incrTag ------ " + JSON.toJSONString(JedisExecutor.execute(jedis -> {
                    //这里使用事务保持原子性
                    Transaction transaction = jedis.multi();
                    transaction.zincrby(tagMonthKey, score, uid);
                    transaction.zincrby(dateKey, score, uid);
                    //只记录本周的所以设置过期时间避免浪费
                    transaction.expire(dateKey, 7 * 24 * 60 * 60);
                    transaction.zincrby(mouthKey, score, uid);
                    transaction.zincrby(tagKey, score, uid);
                    transaction.zincrby(weekKey, score, uid);
                    return transaction.exec();
                }
        )));
    }

    /**
     * 初始化标签排行榜
     *
     * @param tag
     * @param scoreMembers
     */
    public Long tagInit(final String tag, final Map<String, Double> scoreMembers) {
        final String tagKey = KeyGen.TAG_TOP_SCORE.genKey(tag);
        for (int i = 0; i < 10000 - random.nextInt(100); i++) {
            scoreMembers.put(UUID.randomUUID().toString().replaceAll("-", ""), random.nextDouble());
        }
        Long tagResult = JedisExecutor.execute(jedis -> jedis.zadd(tagKey, scoreMembers));
        LOG.info("初始化了 " + tag + " 标签下的" + tagResult + "个成员");
        return tagResult;
    }


    /**
     * 获取便签排行榜指定名次范围人员
     *
     * @param tag
     * @param start
     * @param end
     */
    public void tagTop(final String tag, long start, long end) {
        final String tagKey = KeyGen.TAG_TOP_SCORE.genKey(tag);
        long startTime = System.currentTimeMillis();
        Set<Tuple> set = JedisExecutor.execute(jedis -> jedis.zrevrangeWithScores(tagKey, start, end));
        long endTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(tag + " " + (start + 1) + "——" + (end + 1) + "排行，耗时" + (endTime - startTime) + "毫秒" + lineSeparator);
        sb.append("============================================");
        sb.append(lineSeparator);
        set.forEach(element -> sb.append(element.getElement() + " ---> " + element.getScore() + lineSeparator));
        sb.append("============================================");
        sb.append(lineSeparator);
        LOG.info(sb.toString());
    }

    /**
     * 查询几个标签排行的集合后的排行
     *
     * @param start
     * @param end
     * @param tags
     */
    public void tagsTop(long start, long end, String... tags) {
        String tagsString = Joiner.on(",").join(tags);
        //根据 标签 生成每个标签的key
        String[] tagKeys = new String[tags.length];
        for (int i = 0; i < tags.length; i++) {
            tagKeys[i] = KeyGen.TAG_TOP_SCORE.genKey(tags[i]);
        }
        final String tagKey = KeyGen.TAG_TOP_SCORE.genKey(tagsString);
        long startTime = System.currentTimeMillis();
        Set<Tuple> set = (Set<Tuple>) JedisExecutor.executeWithPipline(jedis -> {
            //并集写入另一个key 中
            jedis.zunionstore(tagKey, tagKeys);
            jedis.zrevrangeWithScores(tagKey, start, end);
        }).get(1);
        long endTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder(tagsString + " " + (start + 1) + "——" + (end + 1) + "排行，耗时" + (endTime - startTime) + "毫秒" + lineSeparator);
        sb.append("============================================");
        sb.append(lineSeparator);
        set.forEach(element -> sb.append(element.getElement() + " ---> " + element.getScore() + lineSeparator));
        sb.append("============================================");
        sb.append(lineSeparator);
        LOG.info(sb.toString());
    }


    /**
     * 今日排行
     * @param start
     * @param end
     */
    public void todayTop(long start, long end) {
        //日期积分
        final String DateKey = KeyGen.DATE_TOP_SCORE.genDateKey(LocalDateTime.now());
        long startTime = System.currentTimeMillis();
        Set<Tuple> set = JedisExecutor.execute(jedis ->
                jedis.zrevrangeWithScores(DateKey, start, end)
        );
        long endTime = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder("今日 " + (start + 1) + "——" + (end + 1) + "排行，耗时" + (endTime - startTime) + "毫秒" + lineSeparator);
        sb.append("============================================");
        sb.append(lineSeparator);
        set.forEach(element -> sb.append(element.getElement() + " ---> " + element.getScore() + lineSeparator));
        sb.append("============================================");
        sb.append(lineSeparator);
        LOG.info(sb.toString());
    }

}
