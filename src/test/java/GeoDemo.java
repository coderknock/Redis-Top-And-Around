import com.alibaba.fastjson.JSON;
import com.coderknock.jedis.KeyGen;
import com.coderknock.jedis.executor.JedisExecutor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import redis.clients.jedis.GeoCoordinate;
import redis.clients.jedis.GeoRadiusResponse;
import redis.clients.jedis.GeoUnit;
import redis.clients.jedis.params.geo.GeoRadiusParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * <p></p>
 *
 * @author 三产
 * @version 1.0
 * @date 2017-09-03
 * @QQGroup 213732117
 * @website http://www.coderknock.com
 * @copyright Copyright 2017 拿客 coderknock.com  All rights reserved.
 * @since JDK 1.8
 */
@DisplayName("提升职业竞争力的必备知识 Redis 附近的人 Demo")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GeoDemo {
    private final String key = KeyGen.GEO.genKey("tour");
    static final Logger LOG = Logger.getLogger(GeoDemo.class.getName());

    /**
     * 初始化数据
     */
    @BeforeAll
    public void init() {
        Map<String, GeoCoordinate> memberCoordinateMap = new HashMap<>();
        memberCoordinateMap.put("三产", new GeoCoordinate(116.300101, 39.881244));
        memberCoordinateMap.put("张三", new GeoCoordinate(116.403963, 39.915119));
        memberCoordinateMap.put("李四", new GeoCoordinate(116.194428, 39.995692));
        memberCoordinateMap.put("王五", new GeoCoordinate(116.278905, 40.000224));
        memberCoordinateMap.put("赵六", new GeoCoordinate(116.461225, 39.925014));

        JedisExecutor.executeNR(jedis -> jedis.geoadd(key, memberCoordinateMap));
    }

    @DisplayName("查询某个人的位置")
    @Test
    public void getOne() {
        List<GeoCoordinate> list = JedisExecutor.execute(jedis -> jedis.geopos(key, "三产", "keke", "张三"));
        LOG.info(JSON.toJSONString(list));
        //没有信息返回 null 
        list = JedisExecutor.execute(jedis -> jedis.geopos(key, "keke"));
        LOG.info(JSON.toJSONString(list));
    }


    @DisplayName("查询某个人的到某个人的距离")
    @Test
    public void query() {
        Double result = JedisExecutor.execute(jedis -> jedis.geodist(key, "三产", "keke", GeoUnit.KM));
        LOG.info(String.valueOf(result));
        result = JedisExecutor.execute(jedis -> jedis.geodist(key, "三产", "张三", GeoUnit.KM));
        LOG.info(String.valueOf(result));
    }

    @DisplayName("查询某个人附近的人")
    @Test
    public void around() {
        List<GeoCoordinate> list = JedisExecutor.execute(jedis -> jedis.geopos(key, "三产"));
        GeoCoordinate geoCoordinate = list.get(0);
        double lon = geoCoordinate.getLongitude();
        double lat = geoCoordinate.getLatitude();
        GeoRadiusParam geoRadiusParam = GeoRadiusParam.geoRadiusParam().withDist().withCoord();
        List<GeoRadiusResponse> geoRadiusResponses = JedisExecutor.execute(jedis -> jedis.georadius(key, lon, lat, 100d, GeoUnit.KM,geoRadiusParam));
        LOG.info(JSON.toJSONString(geoRadiusResponses));
    }


    @DisplayName("删除某个人的位置信息")
    @Test
    public void del() {
        List<GeoCoordinate> list = JedisExecutor.execute(jedis -> {
            jedis.zrem(key, "三产");
            return jedis.geopos(key, "三产", "keke", "张三");
        });
        LOG.info(JSON.toJSONString(list));
    }
}
